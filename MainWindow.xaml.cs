﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace cizhi
{
    /// <summary>
    /// MainWindow.xaml 的交互逻辑
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void BtnNo_MouseEnter(object sender, MouseEventArgs e)
        {
            var rm=new Random();
            var x = rm.Next(0, (int)(this.ActualWidth - btnNo.Width));
            this.btnNo.SetValue(Canvas.LeftProperty,x*1.0 );
            var y = rm.Next(0, (int)(this.ActualHeight - btnNo.Height));
            this.btnNo.SetValue(Canvas.TopProperty,y*1.0 );
        }

        private void BtnYes_Click(object sender, RoutedEventArgs e)
        {
            new Window1().ShowDialog();
        }
    }
}
